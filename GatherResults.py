#!/usr/bin/env python3
import subprocess
import argparse
import time
from math import ceil, floor, log10
import csv
import os
import copy
import numpy as np
from fractions import gcd
from functools import reduce

DUMMY_NUMBER = 999999999

def parse_args():
	parser = argparse.ArgumentParser(description="Create task sets file")

	parser.add_argument('-w', '--workload_location', dest='root_folder', default='Null', 
						action='store', type=str, metavar="WORKLOAD_LOCATION",
						required=True,
						help='The place to pickup task-set files')

	return parser.parse_args()

def run_command(incommand):
	p = subprocess.Popen(incommand.split(), stdout=subprocess.PIPE,
stderr=subprocess.STDOUT)
	outstring = p.stdout.read()
	return outstring

def DispatchTests(directory):
	encoding = 'utf-8'
	for dirName, subdirList, fileList in os.walk(directory):
		
		if len(fileList) > 0:
			print("Executing %s ..."%dirName)
			ResultsPath = directory+"/"+"Results.csv"
			FP = open(ResultsPath, "a+")
			FP.write("# file name, schedulable?, #jobs, #states, #edges, max width, CPU time, memory, timeout, #CPUs, BatchStates\n")
			FP.flush()
			FP.close()
			schedulable_tasksets = 0
			TotalTaskSets = 0
			AnalyzedFiles = 0
			MIN_CPU_TIME  = DUMMY_NUMBER
			MAX_CPU_TIME  = 0
			AVG_CPU_TIME  = 0
			MIN_MEM_USAGE = DUMMY_NUMBER
			MAX_MEM_USAGE = 0
			AVG_MEM_USAGE = 0
			with open(ResultsPath, 'a+') as csvfile:
				ResultWriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
				for Files in range(0, len(fileList)):
					if ("JobResult" in fileList[Files]):
						TotalTaskSets += 1
						print(directory, fileList[Files])
						filepath = directory+"/"+fileList[Files]
						lvFP = open(filepath, 'r')
						data = csv.reader(lvFP, skipinitialspace=True)
						for row in data:
							print(row)
							ResultWriter.writerow(row)
							lvSchedResult = int(row[1])
							schedulable_tasksets += int(row[1])

							if lvSchedResult == 1:
								AnalyzedFiles += 1
								if float(row[6]) < MIN_CPU_TIME:
									MIN_CPU_TIME = float(row[6])
								elif float(row[6]) > MAX_CPU_TIME:
									MAX_CPU_TIME = float(row[6])
								AVG_CPU_TIME += float(row[6])

								if float(row[7]) < MIN_MEM_USAGE:
									MIN_MEM_USAGE = float(row[7])
								elif float(row[7]) > MAX_MEM_USAGE:
									MAX_MEM_USAGE = float(row[7])
								AVG_MEM_USAGE += float(row[7])

						lvFP.close()
						lvCMD = "rm -rf "+filepath
						# run_command(lvCMD)

			FP = open(ResultsPath, "a+")
			lvSTR = "TOTAL_TASKS, SCHED_RATIO, MIN_CPU_TIME, MAX_CPU_TIME, AVG_CPU_TIME, MIN_MEM_USAGE, MAX_MEM_USAGE, AVG_MEM_USAGE,\n"
			FP.write(lvSTR)
			if AnalyzedFiles > 0:
				lvSTR = str(TotalTaskSets)+","+str((schedulable_tasksets/TotalTaskSets)*100)+","+str(MIN_CPU_TIME)+","+str(MAX_CPU_TIME)+","+str((AVG_CPU_TIME/AnalyzedFiles))+","+str(MIN_MEM_USAGE)+","+str(MAX_MEM_USAGE)+","+str((AVG_MEM_USAGE/AnalyzedFiles))+",\n"
			else:
				AnalyzedFiles = len(fileList)
				lvSTR = str(TotalTaskSets)+","+str((schedulable_tasksets/TotalTaskSets)*100)+","+str(MIN_CPU_TIME)+","+str(MAX_CPU_TIME)+","+str((AVG_CPU_TIME/AnalyzedFiles))+","+str(MIN_MEM_USAGE)+","+str(MAX_MEM_USAGE)+","+str((AVG_MEM_USAGE/AnalyzedFiles))+",\n"

			FP.write(lvSTR)
			FP.flush()
			FP.close()

def main():
	opts = parse_args()

	for dirName, subdirList, fileList in os.walk(opts.root_folder):
		if "SOA" in dirName or "FEASIBILITY" in dirName or "Visuals" in dirName:
			continue
		if ("Results" in dirName) and (len(fileList) > 0):
			DispatchTests(dirName)

	print("Finished Collecting Results ... ")

if __name__ == '__main__': 
	main()