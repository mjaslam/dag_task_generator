''' 
*********************** Fork Join DAG Generator (FJDG) **********************
* Author:           Muhammad Junaid Aslam                                   *
* Private Email:    junaidaslam1@gmail.com                                  *
* Work Email:       m.j.aslam@tudelft.nl                                    *
* Rank:             PhD Candidate                                           *
* Institute:        Technical University of Delft, Netherlands              *
*                                                                           *
* This software is a computer program whose purpose is to help the          *
* random generation of directed acyclic graph structures and adding         *
* various properties on those structures.                                   *
*                                                                           *
* ---------------------------------------------------------------------     *
* This software is governed by the Null license under M.J law and     |     *
* abiding by the rules of distribution of free software. You can use, |     *
* modify and redistribute the software under the condition of citing  |     *
* or crediting the authors of this software in your work.             |     *
* ---------------------------------------------------------------------     *
*                                                                           *
* FJDG is a random graph generator:                                         *
* it provides means to generate a directed acyclic graph following a        *
* method of the nested fork join found in "Response-Time Analysis of        *
* Conditional DAG Tasks in Multiprocessor Systems".                         *
*                                                                           *
* This is part of a research project funded by the EWI EEMCS Group of       *
* Technical University of Delft, Netherlands.                               *
*                                                                           *
* Note: In order to generate the visual graphs of DAG Tasks, you need to    *
* install the "graphviz" package, otherwise it will throw error about 'dot' *
* command.                                                                  *
*****************************************************************************

'''
#!/usr/bin/env python3
import argparse
import csv
import numpy as np
import random
import math
import time
import signal, os
import subprocess
import datetime
import sys
import os
import threading
import copy
import random
from fractions import gcd
from functools import reduce

RSC0_COLOR = "blue"
RSC1_COLOR = "black"
RSC2_COLOR = "green"
RSC3_COLOR = "cyan4"
RSC4_COLOR = "violet"
RSC5_COLOR = "red"
RSC6_COLOR = "orange"
RSC7_COLOR = "grey"
RSC8_COLOR = "brown"
RSC9_COLOR = "purple"
RSC10_COLOR = "goldenrod"
CRP_J_COLOR = "saddlebrown"
CRP_W_COLOR = "dodgerblue"


'''
Global Fixed Variables
'''
DUMMY_NUMBER	=	99999
DEFAULT_TASK_EXPANSION_TIMEOUT = 1000 # 1 second(s)
LONGEST_JOB_PATH_COLOR = 11
LONGEST_WCET_PATH_COLOR = 12
IS_TERMINAL =   1
IS_PARALLEL =   2
IS_JOIN     =   3

'''
Specifications of Generation specified @ Command line
'''
DEBUG = 'Off'
WANT_GRAPH = False
WANT_JOBSET = False
WANT_TASKJOB_SET = False
WANT_HETEROGENEOUS  =   False
WANT_CRITICAL_PATH_JOBS  =   False
WANT_CRITICAL_PATH_WCET  =   False
WANT_TASKSET_FILES  =   False
SS_RSC  =   0
SELF_SUSPENDING = False
UTILIZATION_METHOD = ""
RESERVED    =   2
FULL        =   1
CONVERT_TO_NFJ_DAG = 0
FEASIBILITY_ANALYSIS = ""
SCHEDULABILITY_TEST_PATH = ""
FEASIBILITY_ANALYSIS_THREADS = 0
PERIOD_CONDITIONING = False
PERIOD_ASSIGNMENT = ""
PERIODS_ARRAY = []
PRIORITY_ARRAY = []
MULTI_THREADING = False
EQUAL_DEADLINE_TASKS_GENERATION = True
EQUAL_PRIORITY_TASKS_GENERATION = False

'''
Specifications of Task Set Generation specified in TasksetSettings.csv
'''
NR_OF_RUNS = 0
MIN_N = 0 # Minimum Number of Tasks
MAX_N = 0 # Maximum Number of Tasks
TASK_MULTIPLES = 0
RESOURCE_TYPES = 0
CORES_PER_RESOURCE = []
TOTAL_COMPUTING_NODES = 0
MIN_PERIOD = 0
MAX_PERIOD = 0
MAX_HYPER_PERIOD = 0
MAX_PAR_BRANCHES = 0
JITTER_TYPE = "FIXED" # FIXED or VARIABLE
PROB_TERMINAL = 0
PROB_PARALLEL = 0
PROB_ADD_EDGE = 0
MAX_LENGTH = 0
MIN_NODES  = 0
MAX_NODES = 0
MAX_NODES_FOR_ALL_TASKS = 0
MAX_NODE_WCET = 0
MAX_RECURSION_DEPTH = 0
RELEASE_JITTER_R_Min = 0 
RELEASE_JITTER_R_Max = 0
TOTAL_NR_OF_UTILIZATION = 0
UTILIZATION_VECTOR = []
EXEC_TIME_VARIATION = 0
MAX_JOBS_PER_HYPER_PERIOD = 0
RSC_ASSIGNMENT_BY_PROBABILITY = False

class NodeInfo:
	
	def __init__(self): 
		self.TID   = 0
		self.JID   = 0
		self.r_min = 0
		self.r_max = 0
		self.BCET  = 0
		self.WCET  = 0
		self.Pred  = []
		self.Succ  = []
		self.Par_v = []
		self.Desc  = []
		self.Ancs  = []
		self.Term  = False
		self.ResourceType = 1
		self.Deadline = 0
		self.Length = 0

	def DisplayData(self):
		print("T%dJ%d BCET:%d WCET:%d RSC:%d Deadline:%d"%\
			(self.TID, self.JID, self.BCET, self.WCET, self.ResourceType, self.Deadline))

class myThread(threading.Thread):
	def __init__(self, path, Utilization, FA):
		threading.Thread.__init__(self)
		self.path  = path
		self.Utilization  = Utilization
		self.FA = FA
	def run(self):
		print("Starting " + self.path)
		CreateWorkloadRuns(self.path, self.Utilization, self.FA)
		print("Exiting " + self.path)

def getColor(RSC_TYPE):
	if RSC_TYPE == 0:
		return RSC0_COLOR
	elif RSC_TYPE == 1:
		return RSC1_COLOR
	elif RSC_TYPE == 2:
		return RSC2_COLOR
	elif RSC_TYPE == 3:
		return RSC3_COLOR
	elif RSC_TYPE == 4:
		return RSC4_COLOR
	elif RSC_TYPE == 5:
		return RSC5_COLOR
	elif RSC_TYPE == 6:
		return RSC6_COLOR
	elif RSC_TYPE == 7:
		return RSC7_COLOR
	elif RSC_TYPE == 8:
		return RSC8_COLOR
	elif RSC_TYPE == 9:
		return RSC9_COLOR
	elif RSC_TYPE == 10:
		return RSC10_COLOR
	elif RSC_TYPE == LONGEST_JOB_PATH_COLOR:
		return CRP_J_COLOR
	elif RSC_TYPE == LONGEST_WCET_PATH_COLOR:
		return CRP_W_COLOR

def run_command(incommand):
	p = subprocess.Popen(incommand.split(), stdout=subprocess.PIPE,
stderr=subprocess.STDOUT)
	outstring = p.stdout.read()
	return outstring

def getRandomSample(min, max):
	return np.random.random_sample(min, max)

def getRandomInteger(min, max):
	return np.random.random_integers(min, max)

def getSiblingsCount():
	values = []
	p_values = []
	for n in reversed(range(MAX_PAR_BRANCHES)):
		values.append(n+1)
		if n == MAX_PAR_BRANCHES-1:
			lval = (1/MAX_PAR_BRANCHES) + (0.5/MAX_PAR_BRANCHES)
			p_values.append(lval)
		elif n == 0:
			p_values.append(0.5/MAX_PAR_BRANCHES)
		else:
			p_values.append(1/MAX_PAR_BRANCHES)

	return np.random.choice(values, p = p_values)

def getUniform(min, max):
	return np.random.uniform(min, max)

def isTerminalNodeOrParallelSubGraph():
	values = [IS_TERMINAL, IS_PARALLEL, IS_JOIN]
	p_values = [PROB_TERMINAL, PROB_PARALLEL, 1 - PROB_TERMINAL - PROB_PARALLEL]
	return np.random.choice(values, p = p_values)

def getResourcebyProbability():
	values = []
	p_values = []

	for n in range(0, RESOURCE_TYPES):
		RSC = n+1
		values.append(RSC)
		p_values.append((float)(CORES_PER_RESOURCE[n] / TOTAL_COMPUTING_NODES))

	return np.random.choice(values, p = p_values)

def isTerminalNode():
	values = [IS_TERMINAL, IS_JOIN]
	p_values = [PROB_TERMINAL, 1 - PROB_TERMINAL]
	return np.random.choice(values, p = p_values)

def isJoinOrParallelSubGraph():
	values = [IS_PARALLEL, IS_JOIN]
	p_values = [PROB_PARALLEL, 1 - PROB_TERMINAL - PROB_PARALLEL]
	return np.random.choice(values, p = p_values)

def ShouldAddEdge():
	values = [True, False]
	p_values = [PROB_ADD_EDGE, 1 - PROB_ADD_EDGE]
	return np.random.choice(values, p = p_values)

def getRandomResourceAssignment(SelfSuspending = False):
	if SelfSuspending == True:
		return getRandomInteger(0, RESOURCE_TYPES)
	else:
		return getRandomInteger(1, RESOURCE_TYPES)

def getResourceAssignmentbyProbability(SelfSuspending = False):
	'''
	The probability of each resource is calculated as:
		P(sigma_i) = M(sigma_i) / M
		Probability = CoresOfRSC_i / TotalCoresOfAllRSC
	'''
	if SelfSuspending == True:
		# fix this with probability
		return getRandomInteger(0, RESOURCE_TYPES)
	else:
		return getResourcebyProbability()

def RandFixSum(Utilizations_Per_Task, List_of_Nodes_Per_Task):
	nums = []
	for i in totals:
		if i == 0: 
			nums.append([0 for i in range(6)])
			continue
		total = i
		temp = []
		for i in range(5):
			val = np.random.randint(0, total)
			temp.append(val)
			total -= val
		temp.append(total)
		nums.append(temp)

	print(nums)

def PrintNodes(Nodes):
	for n in range(0, len(Nodes)):
		Nodes[n].DisplayData()

def Print_Pred_Succ_Par_Ancs_Desc(VertexID, List, Attribute="Predecessors", PRINT=False):
	printList = "{"

	if (Attribute == "Predecessors"):
		for m in range(0, len(List)):
			printList += "J"+str(List[m])+", "
	elif (Attribute == "Successors"):
		for m in range(0, len(List)):
			printList += "J"+str(List[m])+", "
	else:
		for m in range(0, len(List)):
			printList += "J"+str(List[m].JID)+", "

	printList += "}"

	if PRINT == True:
		print("Vertex:%d %s:%s"%(VertexID, Attribute, printList))

	return printList

def Print_TaskSet(TaskSet, Utilizations, Periods):
	for Task in range(0, len(TaskSet)):
		outAllPaths = []
		AllPaths = []
		CriticalPaths = []
		getAllPaths(TaskSet[Task], TaskSet[Task][0], outAllPaths, AllPaths)
		CriticalPaths, CriticalPaths_WCET = getCriticalPaths_wrt_WCET(outAllPaths)

		print("---CRP_WCET:%d----Util:%f%% = %f-------Period:%d---VOL_G=%d-----"\
			%(CriticalPaths_WCET, Utilizations[Task]/TOTAL_COMPUTING_NODES, Utilizations[Task], Periods[Task], Utilizations[Task]*Periods[Task]))
		
		for Vertex in range(0, len(TaskSet[Task])):
			print("J%d BCET:%d WCET:%d"%(TaskSet[Task][Vertex].JID, TaskSet[Task][Vertex].BCET, TaskSet[Task][Vertex].WCET))

def PrintExtractedParameters():
	lvTotalResources = 0
	print("NR_OF_RUNS:%d"%(NR_OF_RUNS))
	print("MIN_N:%d"%(MIN_N))
	print("MAX_N:%d"%(MAX_N))
	print("TASK_MULTIPLES:%d"%(TASK_MULTIPLES))

	print("RESOURCE_TYPES:%d"%(RESOURCE_TYPES))
	for n in range(0,RESOURCE_TYPES):
		print("CORES_OF_%d = %d"%(n+1,CORES_PER_RESOURCE[n]))
		lvTotalResources += CORES_PER_RESOURCE[n]

	print("RSC_ASSIGNMENT_BY_PROBABILITY:%s"%(RSC_ASSIGNMENT_BY_PROBABILITY))
	print("MIN_PERIOD:%d"%(MIN_PERIOD))
	print("MAX_PERIOD:%d"%(MAX_PERIOD))
	if MAX_HYPER_PERIOD == 0:
		print("MAX_HYPER_PERIOD:%d ... Warning!!! It may allow a large HyperPeriod"%(MAX_HYPER_PERIOD))
	else:
		print("MAX_HYPER_PERIOD:%d"%(MAX_HYPER_PERIOD))
	if MAX_JOBS_PER_HYPER_PERIOD == 0:
		print("MAX_JOBS_PER_HYPER_PERIOD:%d ... Warning!!! It may create large job-set"%(MAX_JOBS_PER_HYPER_PERIOD))
	else:
		print("MAX_JOBS_PER_HYPER_PERIOD:%d"%(MAX_JOBS_PER_HYPER_PERIOD))
	print("MAX_PAR_BRANCHES:%d"%(MAX_PAR_BRANCHES))
	print("JITTER_TYPE:%s"%(JITTER_TYPE))
	print("PROB_TERMINAL:%f"%(PROB_TERMINAL))
	print("PROB_PARALLEL:%f"%(PROB_PARALLEL))
	print("PROB_ADD_EDGE:%f"%(PROB_ADD_EDGE))
	print("MAX_LENGTH:%d"%(MAX_LENGTH))
	print("MIN_NODES:%d"%(MIN_NODES))
	print("MAX_NODES:%d"%(MAX_NODES))
	print("MAX_NODES_FOR_ALL_TASKS:%d"%(MAX_NODES_FOR_ALL_TASKS))
	print("MAX_NODE_WCET:%d"%(MAX_NODE_WCET))
	print("MAX_RECURSION_DEPTH:%d"%(MAX_RECURSION_DEPTH))
	print("RELEASE_JITTER_R_Min:%d"%(RELEASE_JITTER_R_Min))
	print("RELEASE_JITTER_R_Max:%d"%(RELEASE_JITTER_R_Max))

	print("Nr_OF_UTILIZATIONs:%d"%(TOTAL_NR_OF_UTILIZATION))
	for n in range(0, TOTAL_NR_OF_UTILIZATION):
		print("UTILIZATION_%d = %f%%"%(n+1, UTILIZATION_VECTOR[n]))
	
	print("EXEC_TIME_VARIATION:%f"%(EXEC_TIME_VARIATION))
	print("--------------------------------------------")

def isPeriodDuplicate(Periods, inPeriod=0):
	if inPeriod == 0:
		for Period in range(0, len(Periods)):
			if Periods.count(Periods[Period]) > 0:
				if DEBUG == 'e':
					print("Failed for Duplicate Period:%d in List of Periods:"%(Periods[Period]))
					print(Periods)
					print("------------------------------------------------------")	
				return True
	else:	
		if Periods.count(inPeriod) > 0:
			if DEBUG == 'e': 
				print("Failed for Duplicate Period:%d in List of Periods:"%(inPeriod))
				print(Periods)
				print("------------------------------------------------------")		
			return True
	return False

def ExtractParameters(Settings):
	global NR_OF_RUNS
	global MIN_N
	global MAX_N
	global RESOURCE_TYPES
	global CORES_PER_RESOURCE
	global TOTAL_COMPUTING_NODES
	global MIN_PERIOD
	global MAX_PERIOD
	global MAX_HYPER_PERIOD
	global MAX_PAR_BRANCHES
	global JITTER_TYPE 
	global PROB_TERMINAL
	global PROB_PARALLEL
	global PROB_ADD_EDGE
	global MAX_LENGTH
	global MIN_NODES
	global MAX_NODES
	global MAX_NODES_FOR_ALL_TASKS
	global MAX_NODE_WCET
	global MAX_RECURSION_DEPTH
	global RELEASE_JITTER_R_Min
	global RELEASE_JITTER_R_Max
	global TOTAL_NR_OF_UTILIZATION
	global EXEC_TIME_VARIATION
	global TASK_MULTIPLES
	global RSC_ASSIGNMENT_BY_PROBABILITY
	global MAX_JOBS_PER_HYPER_PERIOD

	TotalComputingNodes =   0
	MaxUtilizationPercentage    =   0

	fp = open(Settings, 'r')
	Start = 0
	settings = csv.reader(fp, skipinitialspace=True)
	for row in settings:

		if row[0] != "TypedDagTask" and Start == False:
			print("InvalidFileHeader")
			exit(1)

		Start = True

		if row[0] == "Runs":
			NR_OF_RUNS = int(row[1])
		elif row[0] == "Min_N":
			MIN_N = int(row[1])
		elif row[0] == "Max_N":
			MAX_N = int(row[1])
		elif row[0] == "N_Multiples":
			TASK_MULTIPLES = int(row[1])
		elif row[0] == "ResourceTypes":
			RESOURCE_TYPES = int(row[1])
			for n in range(0,RESOURCE_TYPES):
				CORES_PER_RESOURCE.append(int(row[n+2]))
				TotalComputingNodes += int(row[n+2])
		elif row[0] == "ResourcebyProbability":
			RSC_ASSIGNMENT_BY_PROBABILITY = row[1]
			if RSC_ASSIGNMENT_BY_PROBABILITY == "TRUE":
				RSC_ASSIGNMENT_BY_PROBABILITY = True
			else:
				RSC_ASSIGNMENT_BY_PROBABILITY = False
		elif row[0] == "Min_Period":
			MIN_PERIOD = int(row[1])
		elif row[0] == "Max_Period":
			MAX_PERIOD = int(row[1])
		elif row[0] == "Max_Hyper_Period":
			MAX_HYPER_PERIOD = int(row[1])
		elif row[0] == "Max_Jobs_Per_HP":
			MAX_JOBS_PER_HYPER_PERIOD = int(row[1])
		elif row[0] == "Max_Branches":
			MAX_PAR_BRANCHES = int(row[1])
		elif row[0] == "Jitter_Type":
			JITTER_TYPE = row[1]
		elif row[0] == "Prob_Terminal":
			PROB_TERMINAL = float(row[1])
		elif row[0] == "Prob_Parallel":
			PROB_PARALLEL = float(row[1])
		elif row[0] == "Prob_Add_Edge":
			PROB_ADD_EDGE = float(row[1])
		elif row[0] == "Max_Critical_Path_Nodes":
			MAX_LENGTH = int(row[1])
		elif row[0] == "Min_Nodes":
			MIN_NODES = int(row[1])
		elif row[0] == "Max_Nodes":
			MAX_NODES = int(row[1])
		elif row[0] == "Max_Nodes_For_All_Tasks":
			MAX_NODES_FOR_ALL_TASKS = int(row[1])
		elif row[0] == "Max_Node_WCET":
			MAX_NODE_WCET = int(row[1])
		elif row[0] == "Max_Depth":
			MAX_RECURSION_DEPTH = int(row[1])
		elif row[0] == "ReleaseJitter":
			RELEASE_JITTER_R_Min = int(row[1])
			RELEASE_JITTER_R_Max = int(row[2])
		elif row[0] == "Utilizations":
			TOTAL_NR_OF_UTILIZATION = int(row[1])
			for n in range(0,TOTAL_NR_OF_UTILIZATION):
				UTILIZATION_VECTOR.append(float(row[n+2]))
		elif row[0] == "ExecTimeVariation":
			EXEC_TIME_VARIATION = float(row[1])
		elif row[0] == "EndSpecification":
			print("File Traversing Finished")
			break

		TOTAL_COMPUTING_NODES   =   TotalComputingNodes
		# TOTAL_UTILIZATION =  float(TotalComputingNodes * MaxUtilizationPercentage)

def UUniFast(Number_of_Items, Quantity):
	# Classic UUniFast algorithm:
	Separated_Quantities = []
	sumU = Quantity
	nextSumU = 0

	for i in range(0, Number_of_Items-1):
		nextSumU = sumU * (random.random() ** (1.0 / (Number_of_Items - i)))
		Separated_Quantities.append(sumU - nextSumU)
		sumU = nextSumU
		
	Separated_Quantities.append(sumU)

	return Separated_Quantities

def getTotalTaskWCET(Nodes):
	WCET_SUM = 0
	
	for n in range(0, len(Nodes)):
		WCET_SUM += Nodes[n].WCET

	return WCET_SUM

def getTaskPeriod(utilization, Nodes):
	
	WCET_SUM = 0
	
	for n in range(0, len(Nodes)):
		WCET_SUM += Nodes[n].WCET

	Period = int(WCET_SUM / (utilization))

	return Period

def checkCoPrime(Periods):
	if len(Periods) > 2:
		x = reduce(gcd, Periods)
		return x
	elif len(Periods) == 2:
		return math.gcd(Periods[0], Periods[1])

def csvWriteRow(Writer, List):
	Writer.writerow(List)
	List.clear()

def create_job_set(FileName):
	lvFileName = FileName.split('.')
	lvJobFileName   =   lvFileName[0]+"_Jobs.csv"
	lvPredFileName   =   lvFileName[0]+"_Pred.csv"
	pwd = str(run_command("pwd")).split("\\")
	lvpwd = pwd[0].split("'")

	lvCMD = "python3 "+lvpwd[1]+"/dag-tasks-to-jobs_hetero.py "+FileName+" "+lvJobFileName+" "+lvPredFileName
	res = run_command(lvCMD)

	if WANT_TASKJOB_SET == False:
		lvCMD = "rm -rf "+FileName
		res = run_command(lvCMD)

	return lvJobFileName, lvPredFileName

def create_tasks_file(TaskCount, TaskSetList, FileName, Periods, Priorities):
	TaskSetFileData = []

	with open(FileName, 'w') as csvfile:
		TaskWriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)

		TaskSetFileData.append('R')
		TaskSetFileData.append(RESOURCE_TYPES)
		csvWriteRow(TaskWriter, TaskSetFileData)

		for i in range(0, RESOURCE_TYPES):
			TaskSetFileData.append("M")
			TaskSetFileData.append(CORES_PER_RESOURCE[i])  
			csvWriteRow(TaskWriter, TaskSetFileData)

		TaskSetFileData.append("#")
		csvWriteRow(TaskWriter, TaskSetFileData)

		# A ’T’ row consists of the following columns:
		#
		#   1) ’T’
		#   2) a unique numeric ID
		#   3) the period (in milliseconds, fractional is ok)
		#   4) the relative deadline
		#   5) the priority

		for Task in range(0, TaskCount):
			TaskSetFileData.append('T')
			TaskSetFileData.append(Task+1)
			TaskSetFileData.append(Periods[Task])
			TaskSetFileData.append(Periods[Task])
			TaskSetFileData.append(Priorities[Task])
			csvWriteRow(TaskWriter, TaskSetFileData)

			# A ‘V’ row consists for the following columns (unbounded number):
			#
			#   1) ‘V’
			#   2) task ID to which it belongs
			#   3) a numeric vertex ID (unique w.r.t. its task)
			#   4) earliest release time r^min (relative to start of period, may be zero)
			#   5) latest release time r^max (relative to start of period)
			#   6) BCET
			#   7) WCET
			#   8) ResourceType
			#   9) first predecessor (vertex ID), if any
			#   10) second predecessor (vertex ID), if any
			#   11) third predecessor (vertex ID), if any
			#   … and so on …
			for Node in range(0, len(TaskSetList[Task])):

					TaskSetFileData.append('V')
					TaskSetFileData.append(Task+1)
					TaskSetFileData.append(TaskSetList[Task][Node].JID+1)
					TaskSetFileData.append(TaskSetList[Task][Node].r_min)
					TaskSetFileData.append(TaskSetList[Task][Node].r_max)
					TaskSetFileData.append(TaskSetList[Task][Node].BCET)
					TaskSetFileData.append(TaskSetList[Task][Node].WCET)
					TaskSetFileData.append(TaskSetList[Task][Node].ResourceType)

					for p in range(0, len(TaskSetList[Task][Node].Pred)):
						TaskSetFileData.append(TaskSetList[Task][Node].Pred[p]+1)

					csvWriteRow(TaskWriter, TaskSetFileData)                         
					
def get_hyper_period(numbers):
	return reduce(lambda x, y: (x*y)/gcd(x,y), numbers, 1)     

def getUsedRSCTypes(Nodes):
	UsedRSCTypes = []
	
	for RSC in range(1, RESOURCE_TYPES+1):
		Type_Used = 0
		for v in range(0, len(Nodes)):
			if Nodes[v].ResourceType == RSC:
				Type_Used = RSC
				break

		if Type_Used != 0:
			UsedRSCTypes.append(Type_Used)
			Type_Used = 0

	return UsedRSCTypes
def getNodesCountOfRSCType(Nodes):
	Nodes_Per_RSC_Type = []
	
	for RSC in range(1, RESOURCE_TYPES+1):
		Type_Nodes = 0
		for v in range(0, len(Nodes)):
			if Nodes[v].ResourceType == RSC:
				Type_Nodes += 1
		Nodes_Per_RSC_Type.append(Type_Nodes)
		Type_Nodes = 0

	return Nodes_Per_RSC_Type

def UpdateTaskExecutionTimes(TaskNodes, Utilization, Period):
	# We skip updation of self suspending jobs since their suspenstion time
	# does not count towards system utilization.

	Nodes_Per_RSC_Type = [] # Number of Nodes per RSC Type
	Utilization_Per_RSC_Type = []
	VOL_G_Per_RSC_Type_Nodes = []

	# Get nodes of each type of each task Nodes_Per_RSC_Type = getNodesOfType(Nodes)
	Nodes_Per_RSC_Type = getNodesCountOfRSCType(TaskNodes)

	# Get Utilization_Per_RSC_Type[RSC-1] = Utilization*(CORES_PER_RESOURCE[RSC-1]/TOTAL_COMPUTING_NODES)
	for RSC in range(0, RESOURCE_TYPES):
		Utilization_Per_RSC_Type.append(Utilization*(CORES_PER_RESOURCE[RSC]/TOTAL_COMPUTING_NODES))
	
	# Get WCET of Each type of nodes based on Type_Utilization*Period
	for RSC in range(0, RESOURCE_TYPES):
		VOL_G_Per_RSC_Type_Nodes.append(math.ceil(Utilization_Per_RSC_Type[RSC]*Period))
		

	# Get WCET  for each type of nodes
	for RSC in range(0, RESOURCE_TYPES):
		WCET_Per_Node = UUniFast(Nodes_Per_RSC_Type[RSC], VOL_G_Per_RSC_Type_Nodes[RSC])
		TypeNodeCounter = 0
		for vertex in range(0, len(TaskNodes)):
			if TaskNodes[vertex].ResourceType == RSC+1:
				TaskNodes[vertex].WCET = math.ceil(WCET_Per_Node[TypeNodeCounter])
				TaskNodes[vertex].BCET = math.ceil(TaskNodes[vertex].WCET * EXEC_TIME_VARIATION)
				if TaskNodes[vertex].BCET > TaskNodes[vertex].WCET:
					TaskNodes[vertex].BCET = TaskNodes[vertex].WCET
				TypeNodeCounter += 1

def AssignPeriod(Task, Nodes, Utilization, inPeriods=[]):
	#------------------------------- # Assigning Period to the Task # ------------------------ #
	Period = 0
	ScalingFactor = 1.0
	
	if PERIOD_ASSIGNMENT == "ECRTS_19":
		Period = int(getTaskPeriod(Utilization, Nodes))
	elif PERIOD_ASSIGNMENT == "UNIFORM":
		# Here the periods are automatically scaled
		if EQUAL_DEADLINE_TASKS_GENERATION == True:
			Period = int(random.choice(PERIODS_ARRAY))
		else:			
			while(True):
				Period = int(random.choice(PERIODS_ARRAY))
				if (isPeriodDuplicate(inPeriods, Period) == False): # Check if there is a duplicate period
					break

	if PERIOD_CONDITIONING == True:
		lvWCETSum = getTotalTaskWCET(Nodes)
		if Period < lvWCETSum:
			Period = int(lvWCETSum)

	# Scaling the Period according to given MIN_PERIOD
	if (PERIOD_ASSIGNMENT != "UNIFORM"): # Since in UNIFORM periods we always have multiples of MIN_PERIOD
		if Period < MIN_PERIOD:
			ScalingFactor = float(MIN_PERIOD / Period)
			Period = MIN_PERIOD
			for n in range(0, len(Nodes)):
				Nodes[n].WCET = int(math.ceil(Nodes[n].WCET*ScalingFactor))
				Nodes[n].BCET = int(math.ceil(Nodes[n].BCET*ScalingFactor))
				Nodes[n].r_min = int(math.ceil(Nodes[n].r_min*ScalingFactor))
				Nodes[n].r_max = int(math.ceil(Nodes[n].r_max*ScalingFactor))

				if Nodes[n].BCET > Nodes[n].WCET:
					Nodes[n].BCET = Nodes[n].WCET
				if Nodes[n].r_min > Nodes[n].r_max:
					Nodes[n].r_min = Nodes[n].r_max

		elif Period > MAX_PERIOD:
			if DEBUG == 'e':
				print("Failed for Task:%d Period:%d > MAX_PERIOD:%d"%(Task, Period, MAX_PERIOD))
				print("------------------------------------------------------")
			return Period
		else:
			if (Period % MIN_PERIOD) != 0:
				ScalingFactor = float((Period + (MIN_PERIOD - (Period%MIN_PERIOD))) / Period)
				Period += (MIN_PERIOD - (Period%MIN_PERIOD)) 
				for n in range(0, len(Nodes)):
					Nodes[n].WCET = int(math.ceil(Nodes[n].WCET*ScalingFactor))
					Nodes[n].BCET = int(math.ceil(Nodes[n].BCET*ScalingFactor))
					Nodes[n].r_min = int(math.ceil(Nodes[n].r_min*ScalingFactor))
					Nodes[n].r_max = int(math.ceil(Nodes[n].r_max*ScalingFactor))
				
					if Nodes[n].BCET > Nodes[n].WCET:
						Nodes[n].BCET = Nodes[n].WCET
					if Nodes[n].r_min > Nodes[n].r_max:
						Nodes[n].r_min = Nodes[n].r_max

	# We do it at the end so that period scaling is taken into account
	if (PERIOD_ASSIGNMENT == "UNIFORM") or ((PERIOD_ASSIGNMENT == "ECRTS_19") and (RSC_ASSIGNMENT_BY_PROBABILITY == True)):
		UpdateTaskExecutionTimes(Nodes, Utilization, Period)

	CRP, CRP_WCET = getCriticalPathInfo(Nodes)

	if Period < CRP_WCET:
		if DEBUG == 'e':
			print("Failed for Task:%d Period:%d < CRP_WCET:%d"%(Task, Period, CRP_WCET)) 
			print("------------------------------------------------------")
		Period = 0
		return Period

	return Period

def checkNecessaryCondition(TaskSetList, Utilizations, inputUtilization, Periods=[]):
	'''
	Necessary Condition:
	Ui(RSC) = Sum(Uj(RSC)) | j = 1 to N
	Uj(RSC) = Sum(Ck/Tj) | k = 1 to len(Nodes(RSC))
	Ui(RSC) <= m(RSC)
	'''

	FoundSSNode = False
	TotalNrOfTasks = len(TaskSetList)
	TotalJobsPerHyperPeriod = 0
	TotalNodesOfAllTasks = 0

	if (EQUAL_DEADLINE_TASKS_GENERATION == True):
		for Task in range(0, len(TaskSetList)):
			lvPeriod = 0
			lvPeriod = AssignPeriod(Task, TaskSetList[Task], Utilizations[Task], Periods)
			if lvPeriod == 0 or lvPeriod > MAX_PERIOD:
				return False, TotalJobsPerHyperPeriod
			Periods.append(lvPeriod)

	HyperPeriod = get_hyper_period(Periods)

	for n in range(0, len(Periods)):
		TotalJobsPerHyperPeriod += (HyperPeriod/Periods[n])*len(TaskSetList[n])

	if (MAX_HYPER_PERIOD != 0) and (HyperPeriod > MAX_HYPER_PERIOD):
		if DEBUG == 'e':
			print("Failed for HyperPeriods:%d > MAX_HYPER_PERIOD:%d"%(HyperPeriod, MAX_HYPER_PERIOD))
			print("------------------------------------------------------")
		return False, TotalJobsPerHyperPeriod

	if (MAX_JOBS_PER_HYPER_PERIOD != 0) and (TotalJobsPerHyperPeriod > MAX_JOBS_PER_HYPER_PERIOD): 
		if DEBUG == 'e':
			print("Failed for TotalJobsPerHyperPeriod:%d > MAX_JOBS_PER_HYPER_PERIOD:%d"%(TotalJobsPerHyperPeriod, MAX_JOBS_PER_HYPER_PERIOD))
			print("------------------------------------------------------")
		return False, TotalJobsPerHyperPeriod

	U_RSC = []

	for RSC in range(0, RESOURCE_TYPES): 
		Ui_RSC = 0
		for TaskNodes in range(0, TotalNrOfTasks):
			for Node in range(0, len(TaskSetList[TaskNodes])):
				TotalNodesOfAllTasks = TotalNodesOfAllTasks + 1
				if (SELF_SUSPENDING == True) and (TaskSetList[TaskNodes][Node].ResourceType == SS_RSC):
					FoundSSNode = True
				if TaskSetList[TaskNodes][Node].ResourceType == RSC+1: # Added 1 to Elude Self Suspending
					Ui_RSC +=  (float)(TaskSetList[TaskNodes][Node].WCET / Periods[TaskNodes])
		
		if TotalNodesOfAllTasks > MAX_NODES_FOR_ALL_TASKS:
			if DEBUG == 'e':
				print("Failed for TotalNodesOfAllTasks:%d > MAX_NODES_FOR_ALL_TASKS:%d"%(TotalNodesOfAllTasks, MAX_NODES_FOR_ALL_TASKS))
				print("------------------------------------------------------")
			return False, TotalJobsPerHyperPeriod

		if (inputUtilization > 1) and ((Ui_RSC*inputUtilization) > (CORES_PER_RESOURCE[RSC]*inputUtilization)):
			if DEBUG == 'e':
				print("Failed for RSC:%d Ui_RSC:%f > CORES_PER_RESOURCE[%d]:%d"%(RSC+1, Ui_RSC*inputUtilization, RSC+1, (CORES_PER_RESOURCE[RSC]*inputUtilization)))
				print("------------------------------------------------------")
			return False, TotalJobsPerHyperPeriod
		elif (inputUtilization <= 1) and (Ui_RSC > CORES_PER_RESOURCE[RSC]):
			if DEBUG == 'e':
				print("Failed for RSC:%d Ui_RSC:%f > CORES_PER_RESOURCE[%d]:%d"%(RSC+1, Ui_RSC, RSC+1, CORES_PER_RESOURCE[RSC]))
				print("------------------------------------------------------")
			return False, TotalJobsPerHyperPeriod
		else:
			U_RSC.append(Ui_RSC)
	
	isCoPrime = 10
	isCoPrime = checkCoPrime(Periods)
	if isCoPrime <= 1:
		if DEBUG == 'e':
			print("Failed for Periods being CoPrime.")
			print("------------------------------------------------------")
		return False, TotalJobsPerHyperPeriod

	if (SELF_SUSPENDING == True) and (FoundSSNode == False):
		if DEBUG == 'e':
			print("Failed for No SS_NODE_FOUND")
		return False, TotalJobsPerHyperPeriod

	return True, TotalJobsPerHyperPeriod

def parse_args():

	parser = argparse.ArgumentParser(description="Create task sets file")

	parser.add_argument('-d', '--debug', dest='debug', default='Off', 
						action='store', type=str, metavar="DEBUG",
						required=False,
						help='Choose "d" for DEBUG and "e" for error Messages')

	parser.add_argument('-j', '--job-set', dest='job_set', default='n', 
						action='store', type=str, metavar="Job_Set",
						required=False,
						help="If taskset file created choose Y to create jobset. If you don`t want to remove taskset file, then choose Z with this option\n\
						This option requires 'dag-tasks-to-jobs_hetero.py' script to be placed in the same location as of this DAG generation script")

	parser.add_argument('-ss', '--self_suspending', dest='self_suspending', action='store_const', 
						const=True, required=False,
						help='This option enable self_suspending job generation') 

	parser.add_argument('-l', '--max_jobs_in_path', dest='critical_path', default='n', 
						action='store', type=str, metavar="LONGEST_PATH",
						required=False,
						help="Choose J or W to enable longest path w.r.t max number of jobs and WCET respectively")

	parser.add_argument('-p', '--location', dest='parent_folder', default='~/', 
						action='store', type=str, metavar="STORAGE_LOCATION",
						required=True,
						help='The place to generate folders for saving Workload files')

	parser.add_argument('-f', '--taskset_files', dest='taskset_files', action='store_const', 
						const=True, required=False,
						help='This option Generates Task Set files; If this option is not given taskset data is generated without generating files') 

	parser.add_argument('-g', '--graph', dest='graph', action='store_const', 
						const=True, required=False,
						help='This option generates Graph of DAG Tasks, This option requires "graphviz" utility to be installed') 

	parser.add_argument('-t', '--type', dest='task_type', default='Typed', 
						action='store', type=str, metavar="TYPE",
						required=False,
						help='Specify Task Nature: "Typed" or "Hetero"')

	parser.add_argument('-ed', '--equal_deadline_tasks', dest='equal_deadline_tasks', action='store_const', 
						const=False, required=False,
						help='Choose to disable generation of tasks with equal deadlines / periods.\n\
						Recommended to use -P UNIFORM option with this otherwise it will take a long time.') 

	parser.add_argument('-ep', '--equal_priority_tasks', dest='equal_priority_tasks', action='store_const', 
						const=True, required=False,
						help='Choose to enable generation of tasks with equal priorities w.r.t equal Periods.') 

	parser.add_argument('-mt', '--multi_threading', dest='multi_threading', action='store_const', 
						const=True, required=False,
						help='This option enable one thread per utilization exploration') 

	parser.add_argument('-P', '--period_assignment', dest='period_assignment', default='ECRTS_19', 
						action='store', type=str, metavar="PERIOD_ASSIGNMENT",
						required=False,
						help='Option: "UNIFORM" ... For example, It selects uniformly b/w [MIN_PERIOD, MAX_PERIOD] \n\
						with the interval of MIN_PERIOD and then uses UUnifast to assign WCET per node with respect to \n\
						Utilization generated with UUF or UUFD. The Default option selects period according to ECRTS19 Nasri et al')

	parser.add_argument('-T', '--period_conditioning', dest='period_conditioning', action='store_const', 
						const=True, required=False,
						help='Choose to apply this condition to period: Period = MAX{C/U,C} where C=Sum of WCET of all nodes of a task\n\
						and U=utilization of that task. This condition will be applied before scaling the periods to avoid long HyperPeriods,\n\
						so that scaling is done properly.') 

	parser.add_argument('-n', '--nfj_dag', dest='nfj_dag', default='n', 
						action='store', type=str, metavar="NFJ-DAG",
						required=False,
						help='Choose F to fully convert a non-Nested_Fork_Join (NFJ) DAG into NFJ-DAG\n\
						Choose R to convert without removing conflicting edges on critical path\n\
						Currently it only provides visualization of NFJ_DAG and not the taskset')

	parser.add_argument('-u', '--utilization_method', dest='util_method', default='UUF', 
						action='store', type=str, metavar="UTILIZATION_METHOD",
						required=False,
						help='Specify Utilization Generation Method: UUF (UUnifast) or UUFD (UUnifastDiscard) (Warning: UUFD may take long time)')

	parser.add_argument('-cf', '--check_feasibility', dest='feasibility_check', default='NA', 
						action='store', type=str, metavar="ANALYSIS",
						required=False,
						help='Options: Heterogeneous - Homogeneous - Heterogeneous_Save - Homogeneous_Save .\n\
						Homogeneous_Save_Isolate - Heterogeneous_Save_Isolate - Homogeneous_Create_Feasible - Heterogeneous_Create_Feasible\n\
						Heterogeneous_Save and Homogeneous_Save options will keep the job sets even if task sets are not feasible\n\
						Heterogeneous_Save_Isolate and Homogeneous_Save_Isolate options will save the not feasible job sets in separate directory\n\
						with NOT_FEASIBLE tag. If Homogeneous_Create_Feasible or Heterogeneous_Create_Feasible are chosen then feasible tasksets are created.')

	parser.add_argument('-np', '--schedulability_analysis', dest='nptest', default='NA', 
						action='store', type=str, metavar="np_schedulability_analysis",
						required=False,
						help='Specify path to schedulability_analysis framework binary, It only works if -cf option is used')

	parser.add_argument('-c', '--threads', dest='threads', default='4', 
						action='store', type=str, metavar="NR_OF_THREADS",
						required=False,
						help='Specify number of threads to run schedulability_analysis framework, works only if -np option is used')


	parser.add_argument('Settings', metavar = 'TASKS_SETTINGS_FILE',
		help='Creating Tasks to these param')

	return parser.parse_args()

def SortNodes(Nodes):
	Nodes.sort(key = lambda x:x.JID)

def newNode(TaskNr, NodeNr, Pred):
	Node = NodeInfo()
	Node.TID = TaskNr 
	Node.JID = NodeNr

	if JITTER_TYPE == "FIXED":
		Node.r_min = RELEASE_JITTER_R_Min
		Node.r_max = RELEASE_JITTER_R_Max
	elif JITTER_TYPE == "VARIABLE":
		Node.r_min = RELEASE_JITTER_R_Min
		Node.r_max = getRandomInteger(RELEASE_JITTER_R_Min, RELEASE_JITTER_R_Max) 
	else:
		Node.r_min = 0
		Node.r_max = 0

	if PERIOD_ASSIGNMENT == "ECRTS_19":
		if NodeNr != -1:
			Node.WCET = getUniform(1, MAX_NODE_WCET)
		else:
			Node.WCET = getUniform(1, (MAX_NODE_WCET * EXEC_TIME_VARIATION))

		Node.BCET = math.ceil(Node.WCET * EXEC_TIME_VARIATION)

		if Node.BCET > Node.WCET:
			Node.BCET = Node.WCET
	
	if Pred >= 0:
		Node.Pred.append(Pred)
	if (SELF_SUSPENDING == False) or (NodeNr == -1):
		if RSC_ASSIGNMENT_BY_PROBABILITY == False:
			Node.ResourceType = getRandomResourceAssignment(SelfSuspending = False)
		else:
			Node.ResourceType = getResourceAssignmentbyProbability(SelfSuspending = False)
	else:
		if RSC_ASSIGNMENT_BY_PROBABILITY == False:
			Node.ResourceType = getRandomResourceAssignment(SelfSuspending = True)
		else:
			Node.ResourceType = getResourceAssignmentbyProbability(SelfSuspending = True)

	return Node

def addLegendtoGraph(fp, CriticalPaths_WCET):

	if CriticalPaths_WCET > 0:
		LStr = "label = "+"\""+"I`M LEGEND CRP-WCET:"+str(CriticalPaths_WCET)+"\""+";\n"
	else:
		LStr = "label = "+"\""+"I`M LEGEND"+"\""+";\n"

	header = 'rankdir=LR\n' \
			  'node [shape=plaintext]\n' \
			  'subgraph cluster_01 {\n' + LStr + 'key [label=<<table border="0" cellpadding="0" cellspacing="0" cellborder="0">\n' 

	fp.write("%s"%(header)) 

	for n in range(0, RESOURCE_TYPES+1):
		fp.write('<tr><td align="right" port="i%d">RSC:%d </td></tr>\n'%(n, n))
	fp.write('<tr><td align="right" port="i%d">CRP:%d </td></tr>\n'%(RESOURCE_TYPES+1, RESOURCE_TYPES+1))

	fp.write("</table>>]\n" \
			 'key2 [label=<<table border="0" cellpadding="0" cellspacing="0" cellborder="0">\n'
			)

	for n in range(0, RESOURCE_TYPES+1):
		fp.write('<tr><td port="i%d">&nbsp;</td></tr>\n'%(n))
	fp.write('<tr><td port="i%d">&nbsp;</td></tr>\n'%(RESOURCE_TYPES+1))

	fp.write("</table>>]\n")

	for n in range(0, RESOURCE_TYPES+1):
		lvColor = getColor(n)
		fp.write('key:i%d:e -> key2:i%d:w [color=%s]\n'%(n,n,lvColor))
	if WANT_CRITICAL_PATH_JOBS == True:
		lvColor = getColor(LONGEST_JOB_PATH_COLOR)
		fp.write('key:i%d:e -> key2:i%d:w [color=%s]\n'%(RESOURCE_TYPES+1,RESOURCE_TYPES+1,lvColor))
	elif WANT_CRITICAL_PATH_WCET == True:
		lvColor = getColor(LONGEST_WCET_PATH_COLOR)
		fp.write('key:i%d:e -> key2:i%d:w [color=%s]\n'%(RESOURCE_TYPES+1,RESOURCE_TYPES+1,lvColor))

	fp.write("}\n")

def createTaskGraphFile(Nodes, TaskNr, CriticalPaths, CriticalPaths_WCET, FileName):
	# Creating Nodes File

	if len(Nodes) < 1:
		if DEBUG == 'e':
			print("Failed for creating Graph file due to len(Nodes) < 1")
		return

	lvFileName = FileName.split('.')
	lvstr = lvFileName[0]+"_"+str(TaskNr)+".dot"
	fp = open(lvstr, "w")
	
	fp.write("digraph testing {\n")

	for n in range(0, len(Nodes)):
		nodecolor = getColor(Nodes[n].ResourceType) 
		fontsize = str(10)
		fontcolor = "black"
		JobInfo = "\""+"J"+str(Nodes[n].JID)+" RSC:"+str(Nodes[n].ResourceType)+"\nBCET:"+str(Nodes[n].BCET)+"\nWCET:"+str(Nodes[n].WCET)+"\nDL:"+str(Nodes[n].Length)+"\""
		Label = "[label="+JobInfo+", color="+nodecolor+", fontcolor="+fontcolor+", fontsize="+fontsize+"]"
		fp.write("\tJ%d%s\n"%(Nodes[n].JID, Label))

	fp.write("subgraph Main {\n")

	for n in range(0, len(Nodes)):
		for m in range(0, len(Nodes[n].Pred)):
			if Nodes[n].Pred[m] >= 0:
				fp.write("\tJ%d -> J%d\n"%(Nodes[n].Pred[m], Nodes[n].JID))
	fp.write("}\n")

	if WANT_CRITICAL_PATH_JOBS == True:
		for m in range(0, len(CriticalPaths)):
			fp.write("subgraph CriticalPath_%d {\n"%(m))
	#       print(".*.*.*.*.* Critical Path %d NrNodes:%d WCET:%d .*.*.*.*.*"%(m+1, len(CriticalPaths[m])-1, CriticalPaths[m][len(CriticalPaths[m]) - 1]))
			Edgecolor = getColor(LONGEST_JOB_PATH_COLOR)
			EdgeInfo = "\""+""+"\""
			EdgeLabel = "[label="+EdgeInfo+"color="+Edgecolor+", fontcolor="+Edgecolor+", fontsize="+fontsize+"]"
			for n in range(0, len(CriticalPaths[m])):
				if (CriticalPaths[m][n].JID == 0):
					break 
				elif CriticalPaths[m][n].JID != 0:
					# print("J%d -> J%d"%(CriticalPaths[m][n].JID, CriticalPaths[m][n+1].JID))  
					fp.write("\tJ%d -> J%d %s\n"%(CriticalPaths[m][n+1].JID, CriticalPaths[m][n].JID, EdgeLabel))
			fp.write("}\n")
	
	elif WANT_CRITICAL_PATH_WCET == True:
		for m in range(0, len(CriticalPaths)):
			fp.write("subgraph CriticalPath_%d {\n"%(m))
			# print(".*.*.*.*.* Critical Path %d NrNodes:%d TerminalNode:J%d .*.*.*.*.*"%(m+1, len(CriticalPaths[m])-1, CriticalPaths[m][len(CriticalPaths[m]) - 1].JID))
			Edgecolor = getColor(LONGEST_WCET_PATH_COLOR)
			EdgeInfo = "\""+""+"\""
			EdgeLabel = "[label="+EdgeInfo+"color="+Edgecolor+", fontcolor="+Edgecolor+", fontsize="+fontsize+"]"
			for n in range(0, len(CriticalPaths[m])):
				if (CriticalPaths[m][n].JID == CriticalPaths[m][len(CriticalPaths[m]) - 1].JID):
					break 
				elif CriticalPaths[m][n].JID != CriticalPaths[m][len(CriticalPaths[m]) - 1].JID:
					# print("J%d -> J%d"%(CriticalPaths[m][n].JID, CriticalPaths[m][n+1].JID))  
					fp.write("\tJ%d -> J%d %s\n"%(CriticalPaths[m][n].JID, CriticalPaths[m][n+1].JID, EdgeLabel))
			fp.write("}\n")
	
	addLegendtoGraph(fp, CriticalPaths_WCET)

	fp.write("}")

	fp.close()

	#Convert dot file to Graph File
	lvpng = lvstr.split('.')
	lvCMD = "dot -Tpng "+lvstr+" -o "+lvpng[0]+".png"
	res = run_command(lvCMD)
	lvCMD = "rm "+lvstr
	res = run_command(lvCMD)

def getSelectedChildNodes(ChildSiblings, Nodes):
	SelectedNodes = []
	if len(ChildSiblings) > 0:
		SelectedNodes.append(Nodes[ChildSiblings[0]])        
		if len(ChildSiblings) > 1:
			for n in range(1, len(ChildSiblings)):
				if SelectedNodes[0].WCET < Nodes[ChildSiblings[n]].WCET:
					SelectedNodes.clear()
					SelectedNodes.append(Nodes[ChildSiblings[n]])
				else:
					SelectedNodes.append(Nodes[ChildSiblings[n]])
		else:
			return SelectedNodes

	return SelectedNodes

def getAllPaths(Nodes, InputNode, outAllPaths, AllPaths):
	AllPaths.append(InputNode)
	if len(InputNode.Succ) > 0:
		for m in range(0, len(InputNode.Succ)):
			getAllPaths(Nodes, Nodes[InputNode.Succ[m]], outAllPaths, AllPaths)
			for n in range(0, len(AllPaths)):                
				if AllPaths[n].JID == Nodes[InputNode.Succ[m]].JID:
					del AllPaths[n:]                        
					break
	else:
		Skip = True
		for n in range(0, len(AllPaths) - 1):            
			Skip = True
			for m in range(0, len(AllPaths[n].Succ)):
				if (AllPaths[n].Succ[m] == AllPaths[n+1].JID):# and (AllPaths[n].JID != Nodes[len(Nodes) - 1].JID):
					Skip = False
					break
		if Skip == False:
			# for p in range(0, len(AllPaths)):
			#     print("Node:%d"%(AllPaths[p].JID))
			# print("------------------")
			newPath = AllPaths.copy()
			outAllPaths.append(newPath)
		# else:
		#     print("Path is incomplete")

def getCriticalPaths_wrt_WCET(inAllPaths):
	WCET = 0
	CriticalPaths = []

	WCET_SUM = 0
	
	for n in range(0, len(inAllPaths[0])):
		WCET_SUM += inAllPaths[0][n].WCET
	WCET = WCET_SUM
	CriticalPaths.append(inAllPaths[0])    
	WCET_SUM = 0

	for n in range(1, len(inAllPaths)):
		for m in range(0, len(inAllPaths[n])):
			WCET_SUM += inAllPaths[n][m].WCET
			# print("Node:%d = %d"%(inAllPaths[n][m].JID, WCET))
		if (WCET < WCET_SUM):
			WCET = WCET_SUM
			CriticalPaths.clear()
			CriticalPaths.append(inAllPaths[n])
			WCET_SUM = 0
		elif (WCET == WCET_SUM):
			CriticalPaths.append(inAllPaths[n])            
			WCET_SUM = 0
		else:
			WCET_SUM = 0
		# print("-------WCET:%d---------"%WCET)

	return CriticalPaths, WCET

			  
def getCriticalPathInfo(TaskNodes):
	outAllPaths = []
	AllPaths = []
	CriticalPaths = []

	getAllPaths(TaskNodes, TaskNodes[0], outAllPaths, AllPaths)
	CriticalPaths, CriticalPaths_WCET = getCriticalPaths_wrt_WCET(outAllPaths)

	return CriticalPaths, CriticalPaths_WCET

def getAllLongestJobPathsWithBranches(Nodes, TerminalNode, Length, inPutNodes):
	LongestPathNodes = []
	lvLongestNodes = []
	Max_Length = Length - 1
	Found = False
	
	for n in range(0, len(TerminalNode.Pred)):
		if Max_Length == Nodes[TerminalNode.Pred[n]].Length:
			Found = True

			for m in range(0, len(inPutNodes)):
				LongestPathNodes.append(inPutNodes[m])

			LongestPathNodes.append(Nodes[TerminalNode.Pred[n]])

			if Max_Length >= 1:
				lvLongestNodes = getAllLongestJobPathsWithBranches(Nodes, Nodes[TerminalNode.Pred[n]], Max_Length, LongestPathNodes)
				if len(lvLongestNodes) >= 1:
					for m in range(0, len(lvLongestNodes)):
						LongestPathNodes.append(lvLongestNodes[m])
				else:
					LongestPathNodes.remove(Nodes[TerminalNode.Pred[n]])

	if Found == True:
		return LongestPathNodes
	else:
		DummyNodes = []
		return DummyNodes

def getLongestJobPaths(Nodes, TerminalNode):
	ListsOfLongestPaths = []
	nNodes = []
	nNodes.append(TerminalNode)

	AllLongestPathNodes = getAllLongestJobPathsWithBranches(Nodes, TerminalNode, TerminalNode.Length, nNodes)
	
	PrevID = AllLongestPathNodes[0].JID
	CurrID = PrevID
	listsCounter = 0
	OneLongestPath = [AllLongestPathNodes[0]]
	OneLongestPath.append(AllLongestPathNodes[0])

	for n in range(1, len(AllLongestPathNodes)):
		CurrID = AllLongestPathNodes[n].JID
		if (PrevID != 0) and (CurrID == TerminalNode.JID):
			OneLongestPath.clear()
			OneLongestPath.append(TerminalNode)
			PrevID = CurrID
		elif (PrevID != 0) and (CurrID == 0):
			OneLongestPath.append(AllLongestPathNodes[n])

			Skip = False
			NrOfElementsToCompare = len(OneLongestPath)

			lvMatchedCounter = 0
			for m in range(0, len(ListsOfLongestPaths)):
				for l in range(0, len(ListsOfLongestPaths[m])):
					if OneLongestPath[l].JID == ListsOfLongestPaths[m][l].JID:
						lvMatchedCounter += 1
				if lvMatchedCounter == NrOfElementsToCompare:
					Skip = True
					break
				else:
					lvMatchedCounter = 0
			
			if Skip == False:
				lvList = []
				for m in range(0, len(OneLongestPath)):
					lvList.append(OneLongestPath[m])
				ListsOfLongestPaths.append(lvList)
				listsCounter += 1
			 
			OneLongestPath.clear()
			PrevID = CurrID
		else:
			OneLongestPath.append(AllLongestPathNodes[n])
			PrevID = CurrID

	if (PrevID != 0) and (CurrID == 0):
		
		NrOfElementsToCompare = len(OneLongestPath)

		lvMatchedCounter = 0
		Skip = False

		for m in range(0, len(ListsOfLongestPaths)):
			for l in range(0, len(ListsOfLongestPaths[m])):
				if OneLongestPath[l].JID == ListsOfLongestPaths[m][l].JID:
					lvMatchedCounter += 1
			if lvMatchedCounter == NrOfElementsToCompare:
				Skip = True
				break
			else:
				lvMatchedCounter = 0
		
		if Skip == False:
			lvList = []
			for m in range(0, len(OneLongestPath)):
				lvList.append(OneLongestPath[m])
			ListsOfLongestPaths.append(lvList)
			listsCounter += 1 

	return ListsOfLongestPaths

def getCriticalPaths_wrt_Jobs(AllLongestPaths):

	MAX_WCET_CRITICAL_PATH = 0
	CriticalPaths = []
	for n in range(0, len(AllLongestPaths)):

		lvMaxWCETCriticalPath = 0
		LongestPathNodes = []

		for m in range(0, len(AllLongestPaths[n])):

			lvMaxWCETCriticalPath += AllLongestPaths[n][m].WCET
			LongestPathNodes.append(AllLongestPaths[n][m])

		if MAX_WCET_CRITICAL_PATH < lvMaxWCETCriticalPath:

			MAX_WCET_CRITICAL_PATH = lvMaxWCETCriticalPath
			CriticalPaths.clear()
			LongestPathNodes.append(lvMaxWCETCriticalPath)
			CriticalPaths.append(LongestPathNodes)

		elif MAX_WCET_CRITICAL_PATH == lvMaxWCETCriticalPath:

			LongestPathNodes.append(lvMaxWCETCriticalPath)
			CriticalPaths.append(LongestPathNodes)

	return CriticalPaths, MAX_WCET_CRITICAL_PATH

def updateLengths(Nodes, NodeID, Successors, Length):
	size_l = len(Successors)
	if size_l > 1:
		for n in range(0, size_l):
			if (Successors[n] != -1) and (NodeID != Nodes[Successors[n]].JID):            
				updateLengths(Nodes, Nodes[Successors[n]].JID, Nodes[Successors[n]].Succ, Length+1)
				if (Length + 1) >= Nodes[Successors[n]].Length:
					Nodes[Successors[n]].Length = Length + 1
	elif (size_l == 1) and (NodeID != Nodes[Successors[0]].JID):
		if Successors[0] != -1:
			updateLengths(Nodes, Nodes[Successors[0]].JID, Nodes[Successors[0]].Succ, Length+1)
			if (Length + 1) >= Nodes[Successors[0]].Length:
				Nodes[Successors[0]].Length = Length + 1

def expandSeriesParallelDAG(NodeCounter, Nodes, TaskNr, RootNode, JoiningNode, TermNode, Depth):
	lvNodeCounter = NodeCounter

	if (RootNode.Length >= MAX_LENGTH) or ((len(Nodes)+1) >= MAX_NODES):
		TermNode.Pred.append(RootNode.JID)
		return NodeCounter
	elif (Depth >= MAX_RECURSION_DEPTH):
		JoiningNode.Pred.append(RootNode.JID)
		RootNode.Succ.append(JoiningNode.JID)
		if JoiningNode.Length <= RootNode.Length:
			JoiningNode.Length = RootNode.Length + 1
		return NodeCounter

	# siblings = getRandomInteger(1, MAX_PAR_BRANCHES)

	siblings = getSiblingsCount()
	
	sibling_indices = []
	JoiningNodes = []
	
	# New Joining Node Here
	lvJoiningNode = newNode(TaskNr, -1, -1)

	if WANT_HETEROGENEOUS == True:
		lvJoiningNode.ResourceType = 1

	lvSelfSuspendingFound = False

	for n in range(0, siblings):
		sNode = newNode(TaskNr, lvNodeCounter, RootNode.JID)
		sNode.Length = RootNode.Length + 1
		RootNode.Succ.append(sNode.JID)
		sibling_indices.append(sNode.JID)
		Nodes.append(sNode)
		lvNodeCounter += 1
		
		if SELF_SUSPENDING == True: # Check if one sibling is already self suspending
			if (sNode.ResourceType == SS_RSC) and (lvSelfSuspendingFound == False): #if not Found first SS
				lvSelfSuspendingFound = True
			elif (sNode.ResourceType == SS_RSC) and (lvSelfSuspendingFound == True): #if Yes, ReAssign Resource
				if RSC_ASSIGNMENT_BY_PROBABILITY == False:
					sNode.ResourceType = getRandomResourceAssignment(SelfSuspending = False)
				else:
					sNode.ResourceType = getResourceAssignmentbyProbability(SelfSuspending = False)

		Result = isTerminalNodeOrParallelSubGraph()

		if (Result == IS_TERMINAL) and (sNode.ResourceType != SS_RSC) and (lvNodeCounter >= MIN_NODES):
			TermNode.Pred.append(sNode.JID)
			sNode.Succ.append(TermNode.JID)
		else:
			if WANT_HETEROGENEOUS == True:
				if sNode.ResourceType == 1:
					lvNodeCounter = expandSeriesParallelDAG(lvNodeCounter, Nodes, TaskNr, sNode, lvJoiningNode, TermNode, Depth+1)
				else:
					lvJoiningNode.Pred.append(sNode.JID)
					sNode.Succ.append(lvJoiningNode.JID)
					if lvJoiningNode.Length <= sNode.Length:
						lvJoiningNode.Length = sNode.Length + 1
			else:
				if sNode.ResourceType == SS_RSC:
					lvJoiningNode.Pred.append(sNode.JID)
					sNode.Succ.append(lvJoiningNode.JID)
					if lvJoiningNode.Length <= sNode.Length:
						lvJoiningNode.Length = sNode.Length + 1                    
				else:
					lvNodeCounter = expandSeriesParallelDAG(lvNodeCounter, Nodes, TaskNr, sNode, lvJoiningNode, TermNode, Depth+1)
						

	if len(lvJoiningNode.Pred) >= 1:
		Max_Length = 0
		for n in range(0, len(lvJoiningNode.Pred)):
			if Max_Length <= Nodes[lvJoiningNode.Pred[n]].Length:
				Max_Length = Nodes[lvJoiningNode.Pred[n]].Length

		lvJoiningNode.JID = lvNodeCounter
		lvNodeCounter += 1
		lvJoiningNode.Length = Max_Length + 1
		
		JoiningNode.Pred.append(lvJoiningNode.JID)
		lvJoiningNode.Succ.append(JoiningNode.JID)
		if JoiningNode.Length <= lvJoiningNode.Length:
			JoiningNode.Length = lvJoiningNode.Length + 1

		# Update all the predecessors of lvJoiningNode with new lvJoiningNode ID
		for n in range(0, len(lvJoiningNode.Pred)):
			for m in range(0, len(Nodes[lvJoiningNode.Pred[n]].Succ)):
				if Nodes[lvJoiningNode.Pred[n]].Succ[m] == -1:
					Nodes[lvJoiningNode.Pred[n]].Succ[m] = lvJoiningNode.JID

		Nodes.append(lvJoiningNode)

	# Adding Random Edges between siblings here
	for n in range(0, len(sibling_indices)):
		for m in range(n+1, len(sibling_indices)):
			AddEdge = ShouldAddEdge()
			if AddEdge == True:
				if Nodes[sibling_indices[n]].ResourceType != SS_RSC: # No random edges between self suspending nodes
					Nodes[sibling_indices[m]].Pred.append(Nodes[sibling_indices[n]].JID)
					Nodes[sibling_indices[n]].Succ.append(Nodes[sibling_indices[m]].JID)
					if Nodes[sibling_indices[m]].Length <= Nodes[sibling_indices[n]].Length:
						Nodes[sibling_indices[m]].Length = Nodes[sibling_indices[n]].Length + 1
						updateLengths(Nodes, Nodes[sibling_indices[m]].JID, Nodes[sibling_indices[m]].Succ, Nodes[sibling_indices[m]].Length)

					while True: 
						try:
							TermNode.Pred.remove(Nodes[sibling_indices[n]].JID)
							Nodes[sibling_indices[n]].Succ.remove(TermNode.JID)
						except ValueError:
							break

	return lvNodeCounter

def Generate_DAG_Task(TaskNr, FileName):
	Nodes = []

	begin_timer = int(round(time.time() * 1000))

	while((len(Nodes) < MIN_NODES)):

		NodeCounter = 0

		RootNode = newNode(TaskNr, NodeCounter, -1)
		RootNode.ResourceType = 1
		RootNode.Length = 0
		RootNode.Deadline = 0
		Nodes.append(RootNode)
		
		NodeCounter += 1 

		if MAX_NODES > 1:
			
			Result = isTerminalNode()

			TermNode = newNode(TaskNr, -1, -1)

			NodeCounter = expandSeriesParallelDAG(NodeCounter, Nodes, TaskNr, RootNode, TermNode, TermNode, 1)

			TermNode.JID = NodeCounter
			TermNode.ResourceType = 1

			# Update all the predecessors of TermNode with new TermNode ID
			for n in range(0, len(TermNode.Pred)):
				for m in range(0, len(Nodes[TermNode.Pred[n]].Succ)):
					if Nodes[TermNode.Pred[n]].Succ[m] == -1:
						Nodes[TermNode.Pred[n]].Succ[m] = TermNode.JID

			Max_Length = 0
			for n in range(0, len(TermNode.Pred)):
				if Max_Length <= Nodes[TermNode.Pred[n]].Length:
					Max_Length = Nodes[TermNode.Pred[n]].Length

			TermNode.Length = Max_Length + 1
			Nodes.append(TermNode)

			RSC_Types_Used = getUsedRSCTypes(Nodes)

		if len(RSC_Types_Used) < RESOURCE_TYPES:
			Nodes.clear()
		
	return Nodes

def Vertex_in_List(vertex, List):
	for n in range(0, len(List)):
		if vertex.JID == List[n].JID:
			return True
	return False

def UpdateParallelVertices_of_EachVertex(TaskInfo, AllPaths):
	for vertex in range(0, len(TaskInfo)):
		TaskInfo[vertex].Par_v.clear()
		CommonPaths = []
		UncommonPaths = []

		for path in range(0, len(AllPaths)):
			if (Vertex_in_List(TaskInfo[vertex], AllPaths[path]) == False):
				UncommonPaths.append(AllPaths[path])
			else:
				CommonPaths.append(AllPaths[path])

		for uPath in range(0, len(UncommonPaths)):
			for uVertex in range(0, len(UncommonPaths[uPath])):
				if (UncommonPaths[uPath][uVertex].ResourceType == TaskInfo[vertex].ResourceType):
					SkipVertex = False
					for cPath in range(0, len(CommonPaths)):
						for cVertex in range(0, len(CommonPaths[cPath])):
							if ((UncommonPaths[uPath][uVertex].JID == CommonPaths[cPath][cVertex].JID) == True):
								SkipVertex = True
								break
						if SkipVertex == True:
							break
					if SkipVertex == False:
						if (Vertex_in_List(UncommonPaths[uPath][uVertex], TaskInfo[vertex].Par_v) == False):
							TaskInfo[vertex].Par_v.append(UncommonPaths[uPath][uVertex])
		
		CommonPaths.clear()
		UncommonPaths.clear()   
		TaskInfo[vertex].Par_v.sort(key=lambda v:v.JID)

def UpdateDescendants_of_EachVertex(TaskInfo, AllPaths):
	for vertex in range(0, len(TaskInfo)):   
		TaskInfo[vertex].Desc.clear()
		CommonPaths = []
		for path in range(0, len(AllPaths)):
			if (Vertex_in_List(TaskInfo[vertex], AllPaths[path]) == True):
				CommonPaths.append(AllPaths[path])
						
		for path in range(0, len(CommonPaths)):     
			for uVertex in range(0, len(CommonPaths[path])):
				if CommonPaths[path][uVertex].JID > TaskInfo[vertex].JID:
					if (Vertex_in_List(CommonPaths[path][uVertex], TaskInfo[vertex].Desc) == False):
						TaskInfo[vertex].Desc.append(CommonPaths[path][uVertex])
		CommonPaths.clear()
		TaskInfo[vertex].Desc.sort(key=lambda v:v.JID)

def UpdateAncestors_of_EachVertex(TaskInfo, AllPaths):
	for vertex in range(0, len(TaskInfo)):    
		TaskInfo[vertex].Ancs.clear()
		CommonPaths = []
		for path in range(0, len(AllPaths)):
			if (Vertex_in_List(TaskInfo[vertex], AllPaths[path]) == True):
				CommonPaths.append(AllPaths[path])
						
		for path in range(0, len(CommonPaths)):     
			for uVertex in range(0, len(CommonPaths[path])):
				if CommonPaths[path][uVertex].JID < TaskInfo[vertex].JID:
					if (Vertex_in_List(CommonPaths[path][uVertex], TaskInfo[vertex].Ancs) == False):
						TaskInfo[vertex].Ancs.append(CommonPaths[path][uVertex])
		CommonPaths.clear()
		TaskInfo[vertex].Ancs.sort(key=lambda v:v.JID)

def getUtilizationUUnifastDiscard(Tasks, Utilization):
	UtilizationCondition = False
	UtilizationPerTaskList = []

	while UtilizationCondition == False:
		UtilizationPerTaskList = UUniFast(Tasks, Utilization)       

		for n in range(0, len(UtilizationPerTaskList)):
			if UtilizationPerTaskList[n] > 1:
				UtilizationCondition = False
				if DEBUG == 'e':
					print("Failed for Generated Utilizations Due to Task:%d Utilization:%f > 1 (UUFD)"%(n+1, UtilizationPerTaskList[n]))
				UtilizationPerTaskList.clear()
				break
			else:
				UtilizationCondition = True

	return UtilizationPerTaskList

def getUtilizationUUnifast(Tasks, Utilization):
	return UUniFast(Tasks, Utilization)

def CreateGraphFile(TaskNr, TaskInfo, outAllPaths, FileName):
	AllLongestPaths =[]
	CriticalPaths =[]
	CriticalPaths_WCET  =   0
	
	if len(TaskInfo) >= MIN_NODES:

		if WANT_CRITICAL_PATH_JOBS and (MAX_NODES > 1):
			AllLongestPaths = getLongestJobPaths(TaskInfo, TaskInfo[len(TaskInfo) - 1])
			CriticalPaths, CriticalPaths_WCET = getCriticalPaths_wrt_Jobs(AllLongestPaths)
		elif WANT_CRITICAL_PATH_WCET and (MAX_NODES > 1):                    
			CriticalPaths, CriticalPaths_WCET = getCriticalPaths_wrt_WCET(outAllPaths)

		# print("Task%d --> NodeCount:%d Length:%d TerminalNodes:%d"%(n+1, len(TaskSetList[n]), TaskSetList[n][len(TaskSetList[n]) - 1].Length, len(TaskSetList[n][len(TaskSetList[n]) - 1].Pred)))
		
		createTaskGraphFile(TaskInfo, TaskNr+1, CriticalPaths, CriticalPaths_WCET, FileName)

	return CriticalPaths

def EdgeExists_in_Paths(CriticalPaths, Pred, JobID):
	for m in range(0, len(CriticalPaths)):
		for n in range(0, len(CriticalPaths[m])):
			if (CriticalPaths[m][n].JID == len(CriticalPaths[m])-1):
				break 
			else:
				if (CriticalPaths[m][n].JID == Pred) and (CriticalPaths[m][n+1].JID == JobID):
					return True        
	return False

def RemoveConflictingEdge(VTX, Pred, TaskNodes): 
	Pred.Succ.remove(VTX.JID)
	VTX.Pred.remove(Pred.JID)
	# Update Necessary Task Information
	outAllPaths = []
	AllPaths = []
	getAllPaths(TaskNodes, TaskNodes[0], outAllPaths, AllPaths)
	UpdateAncestors_of_EachVertex(TaskNodes, outAllPaths)

def Convert_to_NFJ_DAG(cNodes, CriticalPaths):
	if DEBUG == 'd':
		print("---------------------------------------")
	for vertex in range(0, len(cNodes)):
		
		if len(cNodes[vertex].Pred) > 1: 
		   
			PredLen = len(cNodes[vertex].Pred)

			for pred in reversed(range(PredLen)):

				lvPredVertex = cNodes[cNodes[vertex].Pred[pred]]
				ConflictingEdge = False

				for succ in range(0, len(lvPredVertex.Succ)):
					if (lvPredVertex.Succ[succ] != cNodes[vertex].JID) \
					and (Vertex_in_List(cNodes[lvPredVertex.Succ[succ]], cNodes[vertex].Ancs) == False):
						if DEBUG == 'd':
							print("Succ:%d of Pred:%d Making %d <-> %d Connection Conflicting"\
								%(cNodes[lvPredVertex.Succ[succ]].JID, lvPredVertex.JID, cNodes[vertex].JID, cNodes[cNodes[vertex].Pred[pred]].JID))
						ConflictingEdge = True
						break

				if (ConflictingEdge == True):
					if CONVERT_TO_NFJ_DAG == RESERVED:
						if EdgeExists_in_Paths(CriticalPaths, lvPredVertex.JID, cNodes[vertex].JID) == False:
							RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[pred]], cNodes)
							PredLen = PredLen - 1
					else:
						if DEBUG == 'd':
							print("Removing Connection between %d <-> %d"%(cNodes[vertex].JID, cNodes[cNodes[vertex].Pred[pred]].JID))
						RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[pred]], cNodes)
						PredLen = PredLen - 1
				
				if ConflictingEdge == False:
					lvPredLen = len(cNodes[vertex].Pred)
					for p in reversed(range(lvPredLen)):
						if cNodes[vertex].Pred[p] in lvPredVertex.Pred:
							if DEBUG == 'd':
								print("Predecessor:%d Common to %d -> %d"%(cNodes[vertex].Pred[p], lvPredVertex.JID, cNodes[vertex].JID))							
								print("Removing Connection between %d <-> %d"%(cNodes[vertex].Pred[p], cNodes[vertex].JID))
							if CONVERT_TO_NFJ_DAG == RESERVED:
								if EdgeExists_in_Paths(CriticalPaths, lvPredVertex.JID, cNodes[vertex].JID) == False:
									RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[p]], cNodes)
									lvPredLen = lvPredLen - 1
									PredLen = PredLen - 1
									if(PredLen == 1):
										break
							else:
								if EdgeExists_in_Paths(CriticalPaths, lvPredVertex.JID, cNodes[vertex].JID) == False:
									RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[p]], cNodes)
									lvPredLen = lvPredLen - 1
									PredLen = PredLen - 1
									if(PredLen == 1):
										break

				if PredLen > 1:
					for n in reversed(range(PredLen)):
						for vtx in reversed(range(PredLen)):
							if (Vertex_in_List(cNodes[cNodes[vertex].Pred[n]], cNodes[cNodes[vertex].Pred[vtx]].Ancs) == True):
								if DEBUG == 'd':
									print("ConflictingEdge:J%d <---> J%d"%(cNodes[cNodes[vertex].Pred[n]].JID, cNodes[vertex].JID))
								if CONVERT_TO_NFJ_DAG == RESERVED:
									if EdgeExists_in_Paths(CriticalPaths, lvPredVertex.JID, cNodes[vertex].JID) == False:
										RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[n]], cNodes)
										PredLen = PredLen - 1
										if(PredLen == 1):
											break
								else:
									RemoveConflictingEdge(cNodes[vertex], cNodes[cNodes[vertex].Pred[n]], cNodes)
									PredLen = PredLen - 1
									if(PredLen == 1):
										break

						if(PredLen == 1):
								break 

				if(PredLen == 1):
					break

def remove_task_jobset(JobSetFileName, PredFileName, FileName=""):
	if FileName != "":
		lvCMD = "rm -rf "+FileName
		res = run_command(lvCMD)
	if JobSetFileName != "":
		lvCMD = "rm -rf "+JobSetFileName
		res = run_command(lvCMD)
	if PredFileName != "":
		lvCMD = "rm -rf "+PredFileName
		res = run_command(lvCMD)

def rename_task_jobset(JobSetFileName, PredFileName, NF_subPath, FileName=""):
	lvJobSet = JobSetFileName.split('.')
	lvPred = PredFileName.split('.')

	if FileName != "":
		lvFileName = FileName.split('.')
		lvCMD = "mv "+FileName+" "+lvFileName[0]+"_NOT_FEASIBLE.csv"
		res = run_command(lvCMD)
		lvCMD = "mv "+lvFileName[0]+"_NOT_FEASIBLE.csv "+NF_subPath+"/"
		res = run_command(lvCMD) 
	
	if JobSetFileName != "":
		lvCMD = "mv "+JobSetFileName+" "+lvJobSet[0]+"_NOT_FEASIBLE.csv"
		res = run_command(lvCMD)
		lvCMD = "mv "+lvJobSet[0]+"_NOT_FEASIBLE.csv "+NF_subPath+"/"
		res = run_command(lvCMD)

	if PredFileName != "":
		lvCMD = "mv "+PredFileName+" "+lvPred[0]+"_NOT_FEASIBLE.csv"
		res = run_command(lvCMD)
		lvCMD = "mv "+lvPred[0]+"_NOT_FEASIBLE.csv "+NF_subPath+"/"
		res = run_command(lvCMD)

def test_feasibility(Type="Heterogeneous", Workload="", threads=4):
	# print("Running feasibility_analysis")
	if (Type == "Heterogeneous") or (Type == "Heterogeneous_Save") or (Type == "Heterogeneous_Save_Isolate") or (Type == "Heterogeneous_Create_Feasible"):
		lvCMD = SCHEDULABILITY_TEST_PATH + " -R hetero --threads="+str(threads)+" "+Workload 
	else:
		lvCMD = SCHEDULABILITY_TEST_PATH + " -m "+ str(CORES_PER_RESOURCE[0]) + " --threads="+str(threads)+" "+Workload 
	if DEBUG == 'd':
		print(lvCMD)
	res = run_command(lvCMD)
	print(res)
	lvres = res.decode().split(',')
	return int(lvres[1])

def ConvertTaskSet_for_Feasibility_Analysis(FA_TaskSetList):
	for Task in range(0, len(FA_TaskSetList)):
		for Vertex in range(0, len(FA_TaskSetList[Task])):
			FA_TaskSetList[Task][Vertex].BCET = FA_TaskSetList[Task][Vertex].WCET

def getTaskSetPriorities(Periods):
	Priorities = []
	Counter = 0

	for n in range(0, len(Periods)):
		Priorities.append(DUMMY_NUMBER)

	for P in range(min(Periods), max(Periods)+MIN_PERIOD, MIN_PERIOD):
		if P in Periods:
			for v in range(0, len(Periods)):
				if (Periods[v] == P):
					Priorities[v] = PRIORITY_ARRAY[Counter]
					Counter += 1

	return Priorities

def createWorkload(Run, Tasks, FileName, Utilization, subPath, GraphFileName):
	FEASIBLE = True

	TaskSetList = []
	UtilizationPerTaskList = []
	
	if UTILIZATION_METHOD == "UUF":
		UtilizationPerTaskList = getUtilizationUUnifast(Tasks, Utilization) 
	else:
		UtilizationPerTaskList = getUtilizationUUnifastDiscard(Tasks, Utilization) 

	NecessaryConditionPass = False
	Periods = []
	Feasibility_Result = True

	while NecessaryConditionPass == False:
		
		SkipNecessaryConditionCheck = False
		TimedOutTask = 0
		Renew_Utilization = False

		for Task in range(1, Tasks+1):                    
			
			TaskGenerated = False
			TaskNodes = []

			begin_timer = int(round(time.time() * 1000))

			while (len(TaskNodes) < MIN_NODES):

				lvPeriod = 0                        
				TaskNodes =   Generate_DAG_Task(Task, FileName)

				if (EQUAL_DEADLINE_TASKS_GENERATION == False):
					lvPeriod = AssignPeriod(Task, TaskNodes, UtilizationPerTaskList[Task-1], Periods)
					if lvPeriod > MAX_PERIOD:
						if DEBUG == 'e':
							print("Failed for lvPeriod:%d Renew Utilization"%lvPeriod)
						Renew_Utilization = True
						break

				if (lvPeriod!=0) and (EQUAL_DEADLINE_TASKS_GENERATION == False): # Check if there is a duplicate period
					if PERIOD_ASSIGNMENT == "ECRTS_19":
						if (isPeriodDuplicate(Periods, lvPeriod) == True):
							TaskNodes.clear()

				if (EQUAL_DEADLINE_TASKS_GENERATION == False):
					if lvPeriod == 0:
						TaskNodes.clear()
						if DEBUG == 'e':
							print("Failed for lvPeriod:%d Cleared TaskNodes"%lvPeriod)
					elif (len(TaskNodes) >= MIN_NODES):
						Periods.append(lvPeriod)
						TaskGenerated = True
					else:
						if DEBUG == 'e':
							print("Failed for Task:%d Nodes:%d < MIN_NODES:%d"%(Task, len(TaskNodes), MIN_NODES))

				if (PERIOD_ASSIGNMENT == "ECRTS_19") and (((int(round(time.time() * 1000)))-begin_timer) <= DEFAULT_TASK_EXPANSION_TIMEOUT):
					break

			if (EQUAL_DEADLINE_TASKS_GENERATION == False):
				if (TaskGenerated == False) and (SkipNecessaryConditionCheck == False) or (Renew_Utilization == True):
					SkipNecessaryConditionCheck = True
					TimedOutTask = Task
					Periods.clear()

			TaskSetList.append(TaskNodes)

		if SkipNecessaryConditionCheck == False: 
			if (EQUAL_DEADLINE_TASKS_GENERATION == False) and (len(Periods) == Tasks):
				NecessaryConditionPass, TotalJobsPerHyperPeriod = checkNecessaryCondition(TaskSetList, UtilizationPerTaskList, (Utilization/TOTAL_COMPUTING_NODES), Periods)
			else:
				NecessaryConditionPass, TotalJobsPerHyperPeriod = checkNecessaryCondition(TaskSetList, UtilizationPerTaskList, (Utilization/TOTAL_COMPUTING_NODES), Periods)
		else:
			if DEBUG == 'e':
				print("Failed for Timeout in Exploring the Task:%d NODES Nr_of_Periods:%d"%(TimedOutTask, len(Periods)))

		if NecessaryConditionPass == False:
			if UTILIZATION_METHOD == "UUF":
				UtilizationPerTaskList = getUtilizationUUnifast(Tasks, Utilization) 
			else:
				UtilizationPerTaskList = getUtilizationUUnifastDiscard(Tasks, Utilization)
			
			TaskSetList.clear()
			Periods.clear()
		else:
			JobSetFileName = "" 
			PredFileName = ""

			if EQUAL_PRIORITY_TASKS_GENERATION == False:
				Priorities = getTaskSetPriorities(Periods)
			else:
				Priorities = Periods

			if WANT_TASKSET_FILES:
				create_tasks_file(Tasks, TaskSetList, FileName, Periods, Priorities)
				if WANT_JOBSET:
					JobSetFileName, PredFileName = create_job_set(FileName)
			else:
				if DEBUG == 'd':
					Print_TaskSet(TaskSetList, UtilizationPerTaskList, Periods)

			if FEASIBILITY_ANALYSIS != "NA":
				TestJobSet = ""
				TestPredFile = ""
				NF_subPath = subPath+"/NOT_FEASIBLE_WORKLOAD/"

				if (FEASIBILITY_ANALYSIS == "Heterogeneous_Save_Isolate") or (FEASIBILITY_ANALYSIS == "Homogeneous_Save_Isolate"):
					try:
						os.mkdir(NF_subPath)
					except OSError:
						if os.path.isdir(NF_subPath) != True:    
							if DEBUG == 'e':                
								print ("Creation of the directory %s failed" % NF_subPath)
							exit(1)
				
				FA_TaskSetList = copy.deepcopy(TaskSetList)

				ConvertTaskSet_for_Feasibility_Analysis(FA_TaskSetList)

				lvFileName = FileName.split('.')
				TestFileName = lvFileName[0]+"_FA.csv"
				
				create_tasks_file(Tasks, FA_TaskSetList, TestFileName, Periods, Priorities)
				TestJobSet, TestPredFile = create_job_set(TestFileName)
				
				Workload = TestJobSet+" -p "+TestPredFile

				if (test_feasibility(FEASIBILITY_ANALYSIS, Workload, FEASIBILITY_ANALYSIS_THREADS) == False):
					
					if DEBUG == 'e':
						print("Failed Necessary Test via schedulability_analysis...")
					
					Feasibility_Result = False

					if (FEASIBILITY_ANALYSIS != "Heterogeneous_Save") and (FEASIBILITY_ANALYSIS != "Homogeneous_Save")\
					 and (FEASIBILITY_ANALYSIS != "Heterogeneous_Save_Isolate") and (FEASIBILITY_ANALYSIS != "Homogeneous_Save_Isolate"):
						remove_task_jobset(JobSetFileName, PredFileName, FileName)
					elif (FEASIBILITY_ANALYSIS == "Heterogeneous_Save_Isolate") or (FEASIBILITY_ANALYSIS == "Homogeneous_Save_Isolate"):
						rename_task_jobset(JobSetFileName, PredFileName, NF_subPath, FileName)				

					FEASIBLE = False
				else:
					FEASIBLE = True
						
				remove_task_jobset(TestJobSet, TestPredFile, TestFileName)
							
	if (WANT_GRAPH == True):

		for n in range(0, len(TaskSetList)):

			outAllPaths = []
			AllPaths = []
			CriticalPaths = []

			getAllPaths(TaskSetList[n], TaskSetList[n][0], outAllPaths, AllPaths)

			UpdateParallelVertices_of_EachVertex(TaskSetList[n], outAllPaths)
			UpdateDescendants_of_EachVertex(TaskSetList[n], outAllPaths) 
			UpdateAncestors_of_EachVertex(TaskSetList[n], outAllPaths)
		
			if (WANT_GRAPH == True):
				CriticalPaths = CreateGraphFile(n, TaskSetList[n], outAllPaths, GraphFileName)

			if CONVERT_TO_NFJ_DAG != False:
				
				cNodes = copy.deepcopy(TaskSetList[n])

				Convert_to_NFJ_DAG(cNodes, CriticalPaths)

				new_outAllPaths = []
				new_AllPaths = []
				getAllPaths(cNodes, cNodes[0], new_outAllPaths, new_AllPaths)

				UpdateParallelVertices_of_EachVertex(cNodes, new_outAllPaths)
				UpdateDescendants_of_EachVertex(cNodes, new_outAllPaths) 
				UpdateAncestors_of_EachVertex(cNodes, new_outAllPaths)
						   
				if (WANT_GRAPH == True):
					lvFileName = GraphFileName.split('.')
					ConvertedFileName = lvFileName[0]+"_NFJ_DAG.csv"
					CreateGraphFile(n, cNodes, new_outAllPaths, ConvertedFileName)
	
	if FEASIBLE:

		lvUtilizationSum = 0
		for Task in range(1, Tasks+1):
			if DEBUG == 'd':
				for RSC in range(0, RESOURCE_TYPES):
					lvCoreRSC = UtilizationPerTaskList[Task-1]*(CORES_PER_RESOURCE[RSC]/TOTAL_COMPUTING_NODES)
					print("Utilization of RSC:%d = %d%% => %f Cores"%(RSC+1, 100*(lvCoreRSC/UtilizationPerTaskList[Task-1]), lvCoreRSC))
				print("Task:%d Nodes:%d Given_Utilization:%f ---> Generated_Utilization:%f WCET_SUM:%d Period:%d"%(Task, len(TaskSetList[Task-1]), UtilizationPerTaskList[Task-1], getTotalTaskWCET(TaskSetList[Task-1])/Periods[Task-1], getTotalTaskWCET(TaskSetList[Task-1]), Periods[Task-1]))
			lvUtilizationSum += (getTotalTaskWCET(TaskSetList[Task-1])/Periods[Task-1])

		print("Generated %d Tasks:: Given Utilization %d%% = %f, Generated Utilization:%d%% = %f Sample_Number:%d = HyperPeriod:%d TotalJobsPerHyperPeriod:%d"\
			%(Tasks, (Utilization/TOTAL_COMPUTING_NODES)*100, Utilization, (lvUtilizationSum/TOTAL_COMPUTING_NODES)*100, lvUtilizationSum, Run, get_hyper_period(Periods), TotalJobsPerHyperPeriod))

	return FEASIBLE

def CreateWorkloadRuns(path, Utilization, FA="NA"):
	fp = 0

	if FA != "NA":
					
		NF_subPath = path+"/FEASIBILITY_ANALYSIS_REPORT/"

		try:
			os.mkdir(NF_subPath)
		except OSError:
			if os.path.isdir(NF_subPath) != True:    
				if DEBUG == 'e':                
					print ("Creation of the directory %s failed" % NF_subPath)
				exit(1)

		Feasibility_Result_File = NF_subPath+"/Report.txt"

		try:
			fp = open(Feasibility_Result_File, "w")
		except IOError:
			if DEBUG == 'e':
				print ("Opening of File %s failed" % Feasibility_Result_File)
				exit(1)

	subPath = ""

	for Tasks in range(MIN_N, MAX_N+TASK_MULTIPLES, TASK_MULTIPLES):

		subPath = path+"Tasks_"+str(Tasks)+"/"
		try:
			os.mkdir(subPath)
		except OSError:
			if os.path.isdir(subPath) != True:    
				if DEBUG == 'e':                
					print ("Creation of the directory %s failed" % subPath)
				exit(1)

		GraphPath = ""
		if WANT_GRAPH == True:
			GraphPath = path+"Tasks_"+str(Tasks)+"/Visuals/"
			try:
				os.mkdir(GraphPath)
			except OSError:
				if os.path.isdir(GraphPath) != True:    
					if DEBUG == 'e':                
						print ("Creation of the directory %s failed" % GraphPath)
					exit(1)

		FEASIBLE = 0
		NOT_FEASIBLE = 0
		FileName = ""
		GraphFileName = ""

		Run = 0 

		while Run < NR_OF_RUNS:

			if WANT_HETEROGENEOUS == True:
				FileName = subPath+"Hetero_Tasks_"+str(Tasks)+"_Run_"+str(Run)+".csv"
				if WANT_GRAPH:
					GraphFileName =  GraphPath+"Hetero_Tasks_"+str(Tasks)+"_Run_"+str(Run)+".csv"
			else:
				FileName = subPath+"Typed_Tasks_"+str(Tasks)+"_Run_"+str(Run)+".csv"
				if WANT_GRAPH:
					GraphFileName =  GraphPath+"Typed_Tasks_"+str(Tasks)+"_Run_"+str(Run)+".csv"
			
			Result = createWorkload(Run, Tasks, FileName, Utilization*TOTAL_COMPUTING_NODES, subPath, GraphFileName)

			if (Result == False):
				NOT_FEASIBLE += 1
			else:
				FEASIBLE += 1

			if (FEASIBILITY_ANALYSIS == "Heterogeneous_Create_Feasible") or (FEASIBILITY_ANALYSIS == "Homogeneous_Create_Feasible"):
				if Result == True:
					Run += 1
			else: 
				Run += 1

		if FEASIBILITY_ANALYSIS != "NA":
			lvSTR = "\nTasks:"+str(Tasks)+" Utilization:"+str(Utilization*100)+"% = "+str(Utilization*TOTAL_COMPUTING_NODES)+\
			" Required_TaskSets:"+str(NR_OF_RUNS)+" Generated:"+str(FEASIBLE+NOT_FEASIBLE)+" => FEASIBLE_TASKSETS:"+str(FEASIBLE)+\
			" NOT_FEASIBLE_TASKSETS:"+str(NOT_FEASIBLE)+" Feasibility_Ratio:"+str(FEASIBLE/(FEASIBLE+NOT_FEASIBLE))+"\n"

			print(lvSTR)

		if fp != 0:
			fp.write(lvSTR)
			fp.flush()

		if (WANT_TASKSET_FILES == False) and (WANT_JOBSET == False) and (WANT_GRAPH == False):
			lvCMD = "rm -rf "+subPath
			run_command(lvCMD)

	if fp != 0:
		fp.close()

def main():

	global DEBUG
	global WANT_GRAPH
	global WANT_JOBSET
	global WANT_HETEROGENEOUS
	global WANT_CRITICAL_PATH_JOBS
	global WANT_CRITICAL_PATH_WCET
	global SELF_SUSPENDING
	global WANT_TASKSET_FILES
	global UTILIZATION_METHOD
	global WANT_TASKJOB_SET
	global CONVERT_TO_NFJ_DAG
	global FEASIBILITY_ANALYSIS
	global SCHEDULABILITY_TEST_PATH
	global FEASIBILITY_ANALYSIS_THREADS
	global PERIOD_CONDITIONING
	global PERIOD_ASSIGNMENT
	global PERIODS_ARRAY
	global MULTI_THREADING
	global EQUAL_DEADLINE_TASKS_GENERATION
	global EQUAL_PRIORITY_TASKS_GENERATION

	opts = parse_args()
	
	WANT_GRAPH 	=	opts.graph

	if(opts.task_type == "Hetero"):
		WANT_HETEROGENEOUS  =   True
	
	if opts.critical_path == 'J': 
		WANT_CRITICAL_PATH_JOBS  = True
	elif opts.critical_path == 'W':
		WANT_CRITICAL_PATH_WCET = True
	
	SELF_SUSPENDING = opts.self_suspending 
 
	if(opts.job_set == 'Y' or opts.job_set == 'y'):
		WANT_JOBSET = True 
	elif(opts.job_set == 'Z' or opts.job_set == 'z'):
		WANT_TASKJOB_SET = True
		WANT_JOBSET = True
	
	WANT_TASKSET_FILES = opts.taskset_files

	if(opts.nfj_dag=='F' or opts.nfj_dag=='f'):
		CONVERT_TO_NFJ_DAG = FULL
	elif(opts.nfj_dag=='R' or opts.nfj_dag=='r'):
		CONVERT_TO_NFJ_DAG = RESERVED 
	
	PERIOD_CONDITIONING = opts.period_conditioning
	
	MULTI_THREADING = opts.multi_threading

	EQUAL_DEADLINE_TASKS_GENERATION = opts.equal_deadline_tasks

	EQUAL_PRIORITY_TASKS_GENERATION = opts.equal_priority_tasks

	DEBUG 							= opts.debug
	PERIOD_ASSIGNMENT 				= opts.period_assignment
	FEASIBILITY_ANALYSIS 			= opts.feasibility_check
	SCHEDULABILITY_TEST_PATH 		= opts.nptest
	FEASIBILITY_ANALYSIS_THREADS 	= int(opts.threads)

	UTILIZATION_METHOD = opts.util_method

	ExtractParameters(opts.Settings)
	PrintExtractedParameters()

	for Task in range(0, MAX_N):
		PRIORITY_ARRAY.append(Task)

	print("Generation of Task Sets:"+ opts.parent_folder)

	if PERIOD_ASSIGNMENT != "ECRTS_19":
		counter = 0
		while (counter < (MAX_PERIOD/MIN_PERIOD)):
			PERIODS_ARRAY.append(MIN_PERIOD*(counter+1))
			counter += 1

	path = opts.parent_folder

	for Utilization in range(0, len(UTILIZATION_VECTOR)):

		if WANT_HETEROGENEOUS == True:
			path = opts.parent_folder+"/"+"HeteroDAGTasks_Util_"+str(int(UTILIZATION_VECTOR[Utilization]*100))+"/"
		else:
			path = opts.parent_folder+"/"+"TypedDAGTasks_Util_"+str(int(UTILIZATION_VECTOR[Utilization]*100))+"/"
		# define the name of the directory to be created
		try:
			os.mkdir(path)
		except OSError:
			if os.path.isdir(path) != True:
				if DEBUG == 'e':
					print ("Creation of the directory %s failed" % path)
				exit(1)

		if MULTI_THREADING:
			try:
				Workloadthread = myThread(path, UTILIZATION_VECTOR[Utilization], FEASIBILITY_ANALYSIS)
				Workloadthread.start()
			except:
				if DEBUG == 'e':
					print("Error: unable to start thread")
		else:
			CreateWorkloadRuns(path, UTILIZATION_VECTOR[Utilization], FEASIBILITY_ANALYSIS)

		if (WANT_TASKSET_FILES == False) and (FEASIBILITY_ANALYSIS == "NA") and (WANT_GRAPH == False):
			lvCMD = "rm -rf "+path
			run_command(lvCMD)

if __name__ == '__main__': 
	main()